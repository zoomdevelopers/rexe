#!/bin/bash

PROJECT_DIR="$HOME/rexe"
LOG_DIR="$PROJECT_DIR/../log"

source "$PROJECT_DIR/app/.env/bin/activate"

if [ ! -d "$LOG_DIR" ]; then
    mkdir -p "$LOG_DIR"
fi

ERROR_LOG="$LOG_DIR/rexe-error.log"
ACCESS_LOG="$LOG_DIR/rexe-access.log"

touch $ERROR_LOG
touch $ACCESS_LOG

cd "$PROJECT_DIR/app/django"

gunicorn app.wsgi:application --access-logformat "%(h)s %(l)s %(u)s %(t)s \"%(r)s\" %(s)s %(b)s \"%(f)s\" \"%(a)s\" %({X-Forwarded-For}i)s" --access-logfile "$ACCESS_LOG" --error-logfile "$ERROR_LOG" --bind 0.0.0.0:8000 --capture-output --workers 4 --timeout 3000 --worker-class gevent --keep-alive 65
