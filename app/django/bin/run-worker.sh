#!/bin/bash

PROJECT_DIR="$HOME/rexe"
LOG_DIR="$PROJECT_DIR/../log"

source "$PROJECT_DIR/app/.env/bin/activate"

if [ ! -d "$LOG_DIR" ]; then
    mkdir -p "$LOG_DIR"
fi

LOG_FILE="$LOG_DIR/rexe-worker.log"

touch $LOG_FILE

cd "$PROJECT_DIR/app/django"

python manage.py celery worker -A app.celery:celery_app --concurrency=4 -f "$LOG_FILE"
